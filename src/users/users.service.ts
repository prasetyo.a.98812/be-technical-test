import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from './users.model';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel('user') private readonly userModel: Model<UserDocument>,
  ) {}
  async createUser(
    username: string,
    password: string,
    role: string,
    email: string,
  ): Promise<User> {
    return this.userModel.create({
      username,
      password,
      role,
      email,
    });
  }
  async getUser(query: object): Promise<User> {
    return this.userModel.findOne(query);
  }
  async updateUser(userId: string, email: string): Promise<User> {
    const existingUsesr = await this.userModel.findByIdAndUpdate(
      userId,
      { email: email },
      { new: true },
    );
    if (!existingUsesr) {
      throw new NotFoundException(`Usesr #${userId} not found`);
    }
    return existingUsesr;
  }
  async deleteUser(userId: string): Promise<User> {
    const deletedUser = await this.userModel.findByIdAndDelete(userId);
    if (!deletedUser) {
      throw new NotFoundException(`User #${userId} not found`);
    }
    return deletedUser;
  }
}
