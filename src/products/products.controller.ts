import {
  Body,
  Controller,
  Post,
  Put,
  Get,
  Param,
  Res,
  HttpStatus,
  Delete,
} from '@nestjs/common';
import { ProductsService } from './products.service';
import { Product } from './products.model';

@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Post('/')
  async createProduct(
    @Body('name') name: string,
    @Body('code') code: string,
  ): Promise<Product> {
    const result = await this.productsService.createProduct(name, code);
    return result;
  }

  @Put('/:id')
  async updateProduct(
    @Res() response,
    @Param('id') productId: string,
    @Body('name') name: string,
  ) {
    try {
      const existingProduct = await this.productsService.updateProduct(
        productId,
        name,
      );
      return response.status(HttpStatus.OK).json({
        message: 'Product has been successfully updated',
        existingProduct,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
  @Get('/:id')
  async getProduct(@Res() response, @Param('id') productId: string) {
    try {
      const existingProduct = await this.productsService.getProduct({
        _id: productId,
      });
      return response.status(HttpStatus.OK).json({
        message: 'Product found successfully',
        existingProduct,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Delete('/:id')
  async deleteProduct(@Res() response, @Param('id') productId: string) {
    try {
      const deletedProduct = await this.productsService.deleteProduct(
        productId,
      );
      return response.status(HttpStatus.OK).json({
        message: 'Product deleted successfully',
        deletedProduct,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
}
