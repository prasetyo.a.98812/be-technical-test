import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Product, ProductDocument } from './products.model';

@Injectable()
export class ProductsService {
  constructor(
    @InjectModel('product')
    private readonly productModel: Model<ProductDocument>,
  ) {}
  async createProduct(name: string, code: string): Promise<Product> {
    return this.productModel.create({
      name,
      code,
    });
  }
  async getProduct(query: object): Promise<Product> {
    return this.productModel.findOne(query);
  }
  async updateProduct(productId: string, name: string): Promise<Product> {
    const existingUsesr = await this.productModel.findByIdAndUpdate(
      productId,
      { name: name },
      { new: true },
    );
    if (!existingUsesr) {
      throw new NotFoundException(`Data #${productId} not found`);
    }
    return existingUsesr;
  }
  async deleteProduct(productId: string): Promise<Product> {
    const deletedProduct = await this.productModel.findByIdAndDelete(productId);
    if (!deletedProduct) {
      throw new NotFoundException(`Product #${productId} not found`);
    }
    return deletedProduct;
  }
}
