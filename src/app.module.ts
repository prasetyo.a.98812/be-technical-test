import { Module, RequestMethod, MiddlewareConsumer } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { ProductsModule } from './products/products.module';
import { WarrantyModule } from './warranty/warranty.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://adminTyo:secret@localhost:27017', {
      dbName: 'authentication',
    }),
    UsersModule,
    AuthModule,
    ProductsModule,
    WarrantyModule,
  ],

  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
