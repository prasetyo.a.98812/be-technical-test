import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Warranty, WarrantyDocument } from './warranty.model';

@Injectable()
export class WarrantyService {
  constructor(
    @InjectModel('warranty')
    private readonly warrantyModel: Model<WarrantyDocument>,
  ) {}
  async createWarranty(
    userId: string,
    product_code: string,
  ): Promise<Warranty> {
    return this.warrantyModel.create({
      userId,
      product_code,
      date: new Date(),
      status: 'submitted',
    });
  }
  async getWarranty(query: object): Promise<Warranty> {
    return this.warrantyModel.findOne(query);
  }
  async updateWarranty(warrantyId: string, status: string): Promise<Warranty> {
    const existingUsesr = await this.warrantyModel.findByIdAndUpdate(
      warrantyId,
      { status: status },
      { new: true },
    );
    if (!existingUsesr) {
      throw new NotFoundException(`Data #${warrantyId} not found`);
    }
    return existingUsesr;
  }
  async deleteWarranty(warrantyId: string): Promise<Warranty> {
    const deletedWarranty = await this.warrantyModel.findByIdAndDelete(
      warrantyId,
    );
    if (!deletedWarranty) {
      throw new NotFoundException(`Warranty #${warrantyId} not found`);
    }
    return deletedWarranty;
  }
}
