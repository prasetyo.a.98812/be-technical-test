import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type WarrantyDocument = Warranty & Document;

@Schema()
export class Warranty {
  @Prop()
  product_code: string;

  @Prop()
  userId: string;

  @Prop()
  status: string;

  @Prop()
  created_at: Date;
}

export const WarrantySchema = SchemaFactory.createForClass(Warranty);
