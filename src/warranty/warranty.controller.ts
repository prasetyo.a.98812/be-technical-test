import {
  Body,
  Controller,
  Post,
  Put,
  Get,
  Param,
  Res,
  HttpStatus,
  Delete,
} from '@nestjs/common';
import { WarrantyService } from './warranty.service';
import { Warranty } from './warranty.model';

@Controller('warranty')
export class WarrantyController {
  constructor(private readonly warrantyService: WarrantyService) {}

  @Post('/')
  async createWarranty(
    @Body('userId') userId: string,
    @Body('product_code') product_code: string,
  ): Promise<Warranty> {
    const result = await this.warrantyService.createWarranty(
      userId,
      product_code,
    );
    return result;
  }

  @Put('/:id')
  async updateWarranty(
    @Res() response,
    @Param('id') warrantyId: string,
    @Body('status') status: string,
  ) {
    try {
      const existingdWarranty = await this.warrantyService.updateWarranty(
        warrantyId,
        status,
      );
      return response.status(HttpStatus.OK).json({
        message: 'Data has been successfully updated',
        existingdWarranty,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
  @Get('/:id')
  async getWarranty(@Res() response, @Param('id') warrantyId: string) {
    try {
      const existingdWarranty = await this.warrantyService.getWarranty({
        _id: warrantyId,
      });
      return response.status(HttpStatus.OK).json({
        message: 'Data found successfully',
        existingdWarranty,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Delete('/:id')
  async deleteWarranty(@Res() response, @Param('id') warrantyId: string) {
    try {
      const deleteWarranty = await this.warrantyService.deleteWarranty(
        warrantyId,
      );
      return response.status(HttpStatus.OK).json({
        message: 'Data deleted successfully',
        deleteWarranty,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
}
